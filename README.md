## Running Backend
The backend depends on docker for isolation.
The docker daemon must be running on the host machine.
https://docs.docker.com/get-docker/
```bash
cd backend
# install python dependencies if necessary
pip install -r backend/requirements.txt
docker build -t fastapi-code-execution .
uvicorn app.main:app --reload --host 0.0.0.0 --port 8000
```

## Running the Frontend

```bash
cd frontend
npm install
npm run dev
```
