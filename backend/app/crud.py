from sqlalchemy.orm import Session
from . import models, schemas

# submit code to the db given the code and result
def create_code_submission(db: Session, code: str, result: str):
    db_code_submission = models.CodeSubmission(code=code, result=result)
    db.add(db_code_submission)
    db.commit()
    db.refresh(db_code_submission)
    return db_code_submission
