import subprocess
import os
import logging
from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel
from sqlalchemy.orm import Session
from .database import SessionLocal, engine
from . import crud, models, schemas

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class CodeRequest(BaseModel):
    code: str

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def execute_code_in_docker(code: str) -> str:
    logger.info("Executing code in Docker")
    try:
        result = subprocess.run(
            [
                'docker', 'run', '--rm', '--network', 'none',
                '-v', f'{os.getcwd()}:/app', 'fastapi-execution-env',
                'python3', '-c', code
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            timeout=10,  # Set a timeout to prevent long-running processes
            text=True
        )
        logger.info(f"Docker execution stdout: {result.stdout}")
        logger.info(f"Docker execution stderr: {result.stderr}")
        if result.returncode != 0:
            logger.error(f"Docker stderr: {result.stderr}")
            raise HTTPException(status_code=400, detail=result.stderr)
        return result.stdout
    except subprocess.TimeoutExpired:
        logger.error("Execution timed out")
        raise HTTPException(status_code=400, detail="Execution timed out")
    except Exception as e:
        logger.error(f"Execution error: {str(e)}")
        raise HTTPException(status_code=400, detail=str(e))

@app.post("/execute")
def execute_code(request: CodeRequest):
    logger.info(f"Received code for execution: {request.code}")
    return {"result": execute_code_in_docker(request.code)}

@app.post("/submit", response_model=schemas.CodeSubmission)
def submit_code(request: CodeRequest, db: Session = Depends(get_db)):
    logger.info(f"Received code for submission: {request.code}")
    result = execute_code_in_docker(request.code)
    code_submission = crud.create_code_submission(db=db, code=request.code, result=result)
    return code_submission
