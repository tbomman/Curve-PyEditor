from sqlalchemy import Column, Integer, String
from .database import Base

class CodeSubmission(Base):
    __tablename__ = "codesubmissions"

    id = Column(Integer, primary_key=True, index=True)
    # raw code as string
    code = Column(String, nullable=False)
    # execution output
    result = Column(String, nullable=False)
