from pydantic import BaseModel

class CodeSubmissionBase(BaseModel):
    code: str
    result: str

class CodeSubmissionCreate(CodeSubmissionBase):
    pass

class CodeSubmission(CodeSubmissionBase):
    id: int

    class Config:
        orm_mode = True
