import React, { useState } from 'react';
import CodeEditor from './CodeEditor';

const CodeExecution: React.FC = () => {
    const [code, setCode] = useState('');
    const [result, setResult] = useState('');
    const [error, setError] = useState('');

    const handleTestCode = async () => {
        try {
            const response = await fetch('/api/execute', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ code }),
            });
            const data = await response.json();
            if (data.error) {
                setError(data.error);
                setResult('');
            } else {
                setResult(data.result);
                setError('');
            }
        } catch (err) {
            setError('An error occurred');
        }
    };

    const handleSubmitCode = async () => {
        try {
            const response = await fetch('/api/submit', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ code }),
            });
            const data = await response.json();
            if (data.error) {
                setError(data.error);
            } else {
                setError('');
                alert('Code submitted successfully!');
            }
        } catch (err) {
            setError('An error occurred');
        }
    };

    return (
        <div className="container mx-auto p-4">
            <CodeEditor code={code} setCode={setCode} />
            <div className="mt-4">
                <button
                    className="bg-blue-500 text-white py-2 px-4 mr-2"
                    onClick={handleTestCode}
                >
                    Test Code
                </button>
                <button
                    className="bg-green-500 text-white py-2 px-4"
                    onClick={handleSubmitCode}
                >
                    Submit
                </button>
            </div>
            <div className="mt-4">
                <h3 className="text-lg font-bold">Result:</h3>
                <pre className="bg-gray-100 p-4">{result}</pre>
            </div>
            {error && <div className="mt-4 text-red-500">{error}</div>}
        </div>
    );
};

export default CodeExecution;
