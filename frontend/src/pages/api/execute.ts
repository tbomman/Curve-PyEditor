import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    const { code } = req.body;

    const response = await fetch('http://localhost:8000/execute', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ code }),
    });

    const data = await response.json();

    if (response.status !== 200) {
        res.status(response.status).json(data);
    } else {
        res.status(200).json(data);
    }
}
