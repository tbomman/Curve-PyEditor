import React from 'react';
import CodeExecution from '../components/CodeExecution';

const Home: React.FC = () => {
  return (
      <div className="container mx-auto p-4">
        <h1 className="text-2xl font-bold mb-4">Python Code Execution</h1>
        <CodeExecution />
      </div>
  );
};

export default Home;
